CREATE TABLE disciplina (
  disciplina_id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  nome VARCHAR(255) NULL,
  descricao VARCHAR(255) NULL,
  codigo_disciplina VARCHAR(45) NULL,
  semestre INTEGER UNSIGNED NULL,
  carga_horaria INTEGER UNSIGNED NULL,
  PRIMARY KEY(disciplina_id)
);

CREATE TABLE ies_geral (
  ies_geral_id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  matricula INTEGER UNSIGNED NOT NULL,
  data_admissao DATE NOT NULL,
  horas_nde INTEGER UNSIGNED NULL,
  orientacao_tcc INTEGER UNSIGNED NULL,
  coordenacao_curso INTEGER UNSIGNED NULL,
  coordenacao_outros_cursos INTEGER UNSIGNED NULL,
  pesquisa_sem_atual INTEGER UNSIGNED NULL,
  ativi_extra_classe_curso INTEGER UNSIGNED NULL,
  ativi_extra_classe_outros_cursos INTEGER UNSIGNED NULL,
  qnt_horas_curso INTEGER UNSIGNED NULL,
  qnt_horas_outros_cursos INTEGER UNSIGNED NULL,
  PRIMARY KEY(ies_geral_id)
);

CREATE TABLE publicacao_comprovantes (
  publicacao_comprovantes INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  documentos_anexados BLOB NULL,
  PRIMARY KEY(publicacao_comprovantes)
);

CREATE TABLE curso (
  curso_id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  tipo VARCHAR(45) NULL,
  modalidade VARCHAR(45) NULL,
  denominacao VARCHAR(255) NULL,
  habilitacao VARCHAR(45) NULL,
  local_oferta VARCHAR(255) NULL,
  turnos_funcionamento VARCHAR(255) NULL,
  numero_vagas_por_turno INTEGER UNSIGNED NULL,
  carga_horaria INTEGER UNSIGNED NULL,
  regime_letivo VARCHAR(45) NULL,
  periodos INTEGER UNSIGNED NULL,
  PRIMARY KEY(curso_id)
);

CREATE TABLE professor (
  professor_id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  curso_id INTEGER UNSIGNED NOT NULL,
  nome VARCHAR(255) NOT NULL,
  cpf INTEGER UNSIGNED NOT NULL,
  maior_titulacao VARCHAR(45) NULL,
  area_formacao VARCHAR(255) NULL,
  link_curriculo VARCHAR(255) NULL,
  data_atualizacao VARCHAR(45) NULL,
  PRIMARY KEY(professor_id),
  INDEX professor_FKIndex1(curso_id),
  FOREIGN KEY(curso_id)
    REFERENCES curso(curso_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

CREATE TABLE ies (
  ies_id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  ies_geral_id INTEGER UNSIGNED NOT NULL,
  professor_id INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY(ies_id),
  INDEX atuacao_ies_FKIndex1(professor_id),
  INDEX atuacao_ies_FKIndex2(ies_geral_id),
  FOREIGN KEY(professor_id)
    REFERENCES professor(professor_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(ies_geral_id)
    REFERENCES ies_geral(ies_geral_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

CREATE TABLE bibliografia (
  bibliografia_id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  curso_id INTEGER UNSIGNED NOT NULL,
  disciplina_id INTEGER UNSIGNED NOT NULL,
  titulo VARCHAR(45) NULL,
  autor VARCHAR(45) NULL,
  isbn VARCHAR(45) NULL,
  ano DATE NULL,
  editora VARCHAR(255) NULL,
  PRIMARY KEY(bibliografia_id),
  INDEX bibliografia_FKIndex1(disciplina_id),
  INDEX bibliografia_FKIndex2(curso_id),
  FOREIGN KEY(disciplina_id)
    REFERENCES disciplina(disciplina_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(curso_id)
    REFERENCES curso(curso_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

CREATE TABLE plano_ensino (
  plano_ensino_id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  bibliografia_id INTEGER UNSIGNED NOT NULL,
  professor_id INTEGER UNSIGNED NOT NULL,
  disciplina_id INTEGER UNSIGNED NOT NULL,
  curso_id INTEGER UNSIGNED NOT NULL,
  carga_horaria INTEGER UNSIGNED NULL,
  periodo_do_curso INTEGER UNSIGNED NULL,
  ementa VARCHAR(255) NULL,
  competencias_habilidades VARCHAR(255) NULL,
  metodologia_ensino VARCHAR(255) NULL,
  avaliacao VARCHAR(255) NULL,
  bibliografia VARCHAR(255) NULL,
  PRIMARY KEY(plano_ensino_id),
  INDEX plano_ensino_FKIndex1(curso_id),
  INDEX plano_ensino_FKIndex2(disciplina_id),
  INDEX plano_ensino_FKIndex3(professor_id),
  INDEX plano_ensino_FKIndex4(bibliografia_id),
  FOREIGN KEY(curso_id)
    REFERENCES curso(curso_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(disciplina_id)
    REFERENCES disciplina(disciplina_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(professor_id)
    REFERENCES professor(professor_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(bibliografia_id)
    REFERENCES bibliografia(bibliografia_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

CREATE TABLE plano_ensino_cronograma (
  plano_ensino_cronograma_id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  plano_ensino_id INTEGER UNSIGNED NOT NULL,
  aula INTEGER UNSIGNED NULL,
  conteudo VARCHAR(255) NULL,
  PRIMARY KEY(plano_ensino_cronograma_id),
  INDEX plano_ensino_cronograma_FKIndex1(plano_ensino_id),
  FOREIGN KEY(plano_ensino_id)
    REFERENCES plano_ensino(plano_ensino_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

CREATE TABLE ies_atuacao_profissional (
  ies_atuacao_profissional_id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  ies_id INTEGER UNSIGNED NOT NULL,
  membro_nde BIT NULL,
  membro_colegiado BIT NULL,
  formacao_capac_expPedagogica BIT NULL,
  docente_curso_data_inicial DATE NULL,
  docente_curso_tempo_total INTEGER UNSIGNED NULL,
  exp_magisterio_data_inicial DATE NULL,
  exp_magisterio_tempo_total INTEGER UNSIGNED NULL,
  exp_curso_ead_data_inicial DATE NULL,
  exp_curso_ead_tempo_total INTEGER UNSIGNED NULL,
  tempo_exp_profis_data_inicial DATE NULL,
  tempo_exp_profissional_tempo_total INTEGER UNSIGNED NULL,
  participacao_eventos INTEGER UNSIGNED NULL,
  PRIMARY KEY(ies_atuacao_profissional_id, ies_id),
  INDEX ies_atuacao_profissional_FKIndex1(ies_id),
  FOREIGN KEY(ies_id)
    REFERENCES ies(ies_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

CREATE TABLE ies_publicacao (
  ies_publicacao_id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  ies_id INTEGER UNSIGNED NOT NULL,
  artigos_area INTEGER UNSIGNED NULL,
  artigos_outras_areas INTEGER UNSIGNED NULL,
  livros_cap_area INTEGER UNSIGNED NULL,
  livros_cap_outras_areas INTEGER UNSIGNED NULL,
  trab_completos INTEGER UNSIGNED NULL,
  trab_resumos INTEGER UNSIGNED NULL,
  prop_intelectual_depositado INTEGER UNSIGNED NULL,
  prop_intelectual_registrado INTEGER UNSIGNED NULL,
  outros_traducao_cap_artigos INTEGER UNSIGNED NULL,
  outros_proj_prod_tecnico_art_cult INTEGER UNSIGNED NULL,
  outros_producao_didat_pub_n INTEGER UNSIGNED NULL,
  PRIMARY KEY(ies_publicacao_id),
  INDEX ies_publicacao_FKIndex1(ies_id),
  FOREIGN KEY(ies_id)
    REFERENCES ies(ies_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

CREATE TABLE ies_publicacao_has_publicacao_comprovantes (
  ies_publicacao_id INTEGER UNSIGNED NOT NULL,
  publicacao_comprovantes_publicacao_comprovantes INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY(ies_publicacao_id, publicacao_comprovantes_publicacao_comprovantes),
  INDEX ies_publicacao_has_publicacao_comprovantes_FKIndex1(ies_publicacao_id),
  INDEX ies_publicacao_has_publicacao_comprovantes_FKIndex2(publicacao_comprovantes_publicacao_comprovantes),
  FOREIGN KEY(ies_publicacao_id)
    REFERENCES ies_publicacao(ies_publicacao_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(publicacao_comprovantes_publicacao_comprovantes)
    REFERENCES publicacao_comprovantes(publicacao_comprovantes)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

CREATE TABLE participacao_eventos (
  participacao_eventos_id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  ies_atuacao_profissional_ies_id INTEGER UNSIGNED NOT NULL,
  ies_atuacao_profissional_id INTEGER UNSIGNED NOT NULL,
  nome_evento VARCHAR(255) NULL,
  PRIMARY KEY(participacao_eventos_id),
  INDEX participacao_eventos_FKIndex1(ies_atuacao_profissional_id, ies_atuacao_profissional_ies_id),
  FOREIGN KEY(ies_atuacao_profissional_id, ies_atuacao_profissional_ies_id)
    REFERENCES ies_atuacao_profissional(ies_atuacao_profissional_id, ies_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);


