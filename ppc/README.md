# Sistema PPC

Sistema de software para gestão do Plano Pedagógico de Cursos.
Projeto Integrador 3 - Grupo 2 - Curso de Análise e Desenvolvimento de Sistemas | IESB

## Introdução

Sistema web para gestão do Plano Pedagógico de Ensino.

### Pré-requisitos

O que você precisa para instalar o software e como instalá-los

```
exemplos
```

### Instalação

Uma série passo a passo de exemplos que lhe dizem que você deve ter um desenvolvimento env em execução

```
exemplos
```

repetir até

```
finalizar
```

## Rodando os testes

Explique como executar os testes automatizados para este sistema

## Deployment

Adicione notas adicionais sobre como implantar isso.

## Construido com

* [Java 8] (http://www.oracle.com/technetwork/pt/java/javaee/overview/index.html)
* [Spring] (https://spring.io/)
* [Maven](https://maven.apache.org/) - Dependency Management
* [HTML 5] (https://www.w3schools.com/html/html5_intro.asp)
* [BootStrap 3] (http://getbootstrap.com/)
* [JavaScript] (https://www.javascript.com/)

## Autores

* **Marks Duarte** - [MarksDuarte](https://gitlab.com/marksduarte)
* **Thais Viana** - [ThaisViana](https://gitlab.com/thaisgo)
* **Iria Luna** - [IriaLuna] (https://gitlab.com/irialuna)
* **Vinicio Eto** - [VinicioEto] (https://gitlab.com/vinicioeto)

## Licença

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


