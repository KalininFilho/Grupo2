package com.ads.semestre3.ppc.repository;

import com.ads.semestre3.ppc.model.Curso;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Cursos extends JpaRepository<Curso, Long> {

}
