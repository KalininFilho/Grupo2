package com.ads.semestre3.ppc.controller;

import com.ads.semestre3.ppc.model.Usuario;
import com.ads.semestre3.ppc.repository.Usuarios;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author marks duarte
 */

@Controller
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    Usuarios usuarios;

    @RequestMapping("/login")
    public ModelAndView login() {
        return new ModelAndView("Login");
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView acessarSistema(Usuario usuario, HttpSession session) {
        Usuario usuarioEncontrado = null;
        ModelAndView mv = new ModelAndView();

        List<Usuario> usuarioList = usuarios.findAll();
        for (Usuario u : usuarioList) {
            if (u.getUserName().equals(usuario.getUserName()) && u.getPassword().equals(usuario.getPassword())) {
                usuarioEncontrado = u;
            }
        }

        if (usuarioEncontrado != null) {
            session.setAttribute("usuarioLogado", usuario.getUserName());
            mv.setViewName("Home");
            System.out.println("Usuário logado: " + usuario.getUserName());
            return mv;
        } else {
            mv.setViewName("Login");
            mv.addObject("mensagemErro", "Nome de usuário ou senha inválido.");
            return mv;
        }
    }

    @RequestMapping(value = "/cadastro", method = RequestMethod.POST)
    public ModelAndView novoUsuario(Usuario usuario){
        ModelAndView mv = new ModelAndView();
        if(usuario.getPassword().equals(usuario.getConfirmPassword())){
            usuarios.save(usuario);
            mv.setViewName("Login");
            mv.addObject("mensagemSucesso", "Usuário cadastrado com sucesso!");
            System.out.println("Usuário cadastrado: "+usuario.getUserName());
            return mv;
        } else {
            mv.setViewName("Login");
            mv.addObject("mensagemErro","Erro ao cadastrar usuário.");
            return mv;
        }
    }

    @RequestMapping(value = "/logout")
    public ModelAndView logout(HttpSession session){
        session.invalidate();
        return new ModelAndView("Login");
    }
}
