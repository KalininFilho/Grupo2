package com.ads.semestre3.ppc.controller;

import com.ads.semestre3.ppc.model.Curso;
import com.ads.semestre3.ppc.model.Professor;
import com.ads.semestre3.ppc.repository.Cursos;
import com.ads.semestre3.ppc.repository.Professores;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/curso")
public class CursoController {

    private static final String CADASTRO_CURSO = "CadastroCurso";

    @Autowired
    private Cursos cursos;

    @Autowired
    private Professores professores;

    @RequestMapping("/cadastro")
    public ModelAndView curso(){
        ModelAndView mv = new ModelAndView(CADASTRO_CURSO);
        mv.addObject(new Curso());
        mv.addObject("todosProfessores", professores.findAll());
        return mv;
    }

    @RequestMapping(value = "/cadastro", method = RequestMethod.POST)
    public String cadastroCurso(@Validated Curso curso, Errors errors, RedirectAttributes attributes){
        if(errors.hasErrors()){
            return CADASTRO_CURSO;
        }
        cursos.save(curso);
        attributes.addFlashAttribute("mensagem","Curso cadastrado com sucesso!");
        return "redirect:/curso/cadastro";
    }
}
