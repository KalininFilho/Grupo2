package com.ads.semestre3.ppc.repository;

import com.ads.semestre3.ppc.model.Professor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Professores extends JpaRepository<Professor, Long> {
}

