package com.ads.semestre3.ppc.controller;

import com.ads.semestre3.ppc.model.CargaHoraria;
import com.ads.semestre3.ppc.model.Disciplina;
import com.ads.semestre3.ppc.repository.Disciplinas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/disciplina")
public class DisciplinasController {

    @Autowired
    private Disciplinas disciplinas;

    @RequestMapping("/cadastro")
    public ModelAndView nova() {
        ModelAndView mv = new ModelAndView("CadastroDisciplina");
        mv.addObject(new Disciplina());
        return mv;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView salvar(Disciplina disciplina) {
        disciplinas.save(disciplina);
        ModelAndView mv = new ModelAndView("CadastroDisciplina");
        mv.addObject("mensagem", "Disciplina cadastrada com sucesso!");
        return mv;
    }

    @ModelAttribute("todasCargasHorarias")
    public List<CargaHoraria> todasCargasHorarias() {
        return Arrays.asList(CargaHoraria.values());
    }

    @ModelAttribute("todasDisciplinas")
    public List<Disciplina> todasDisciplinas(){
        return disciplinas.findAll();
    }

}
